<?php


if ( ! function_exists( 'ziegenhagel_register_navigations' ) ) {
	/**
	 * Registers theme's navigation menus.
	 *
	 * @todo Change function prefix to your textdomain.
	 * @todo Update prefix in the hook function and if statement.
	 *
	 * @return void
	 */
	function ziegenhagel_register_navigations() {
		register_nav_menus(
			array(
				'top'    => __( 'Top Navigation', 'ziegenhagel' ),
				'bottom' => __( 'Bottom Navigation', 'ziegenhagel' ),
				'action' => __( 'Action Navigation', 'ziegenhagel' ),
				'sitemap' => __( 'Sitemap Navigation', 'ziegenhagel' ),
			)
		);
	}
}
add_action( 'after_setup_theme', 'ziegenhagel_register_navigations' );

if ( ! function_exists( 'ziegenhagel_register_thumbnails' ) ) {
	/**
	 * Registers theme's additional thumbnail sizes.
	 *
	 * @todo Change function prefix to your textdomain.
	 * @todo Update prefix in the hook function and if statement.
	 *
	 * @return void
	 */
	function ziegenhagel_register_thumbnails() {
		add_image_size( 'custom-thumbnail', 800, 600, true );
	}
}
add_action( 'after_setup_theme', 'ziegenhagel_register_thumbnails' );

add_theme_support( 'post-thumbnails' );
