<div class="page bg-white/60 backdrop-blur p-5 mt-20 ">
<h1 class="title">
    <?php the_title(); ?>
</h1>
<?php the_content(); ?>
