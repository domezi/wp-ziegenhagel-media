<nav id="bottom" class="block md:flex backdrop-blur uppercase items-center bg-black/60 p-5 justify-end text-white font-bold text-xl mt-20" >


    <?php
    wp_nav_menu( array(
        'theme_location' => 'bottom' ,
        "depth" => 1,)
    );
?>
</nav>


