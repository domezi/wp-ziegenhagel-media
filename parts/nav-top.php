<nav id="top" class="flex backdrop-blur uppercase items-center bg-black/60 p-5 justify-between text-white font-bold text-xl" >

    <div class="flex mobile-top items-center">
        <a href="<?php echo get_home_url();?>" class="text-bold">
            Baumrakete
            <div class="text-white/50">
            Joris Felenda
            </div>
        </a>

         <label for="a" class="hamburger"> <div></div> <div></div> <div></div> </label>
     </div>

    <input type="checkbox" id="a" />
    <?php
        wp_nav_menu( array(
            'theme_location' => 'top' ,
            "depth" => 1,)
        );
    ?>

</nav>
