<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1">
	<meta http-equiv="X-UA-Comorange patible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri().'?'.time();?>"/>
	<title><?php wp_title( '-', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<?php wp_head(); ?>
</head>
	<body>
	<?php if(!is_front_page()) get_template_part( 'parts/nav', 'top' ); ?>


<?php
// if user is logged in
if ( is_user_logged_in() ) {
    // if user is admin
    if ( current_user_can( 'manage_options' ) ) {
        // show additioanl css
        echo '<style>
        #top {
            top: 30px
        }
        </style>';
    }
}

?>
