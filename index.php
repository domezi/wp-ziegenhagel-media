<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php if ( is_front_page() ) : ?>
         <?php get_template_part( 'templates/home' ); ?>
    <?php else : ?>
         <?php while ( have_posts() ) : ?>
             <?php the_post(); ?>
             <?php get_template_part( 'templates/' . get_post_type() ); ?>
         <?php endwhile; ?>
    <?php endif; ?>
<?php endif; ?>

<?php get_footer(); ?>
